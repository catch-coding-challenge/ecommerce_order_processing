<?php

namespace App\Tests\Services;

use App\Services\AmazonS3Service;
use App\Services\EcommerceOrder;
use PHPUnit\Framework\TestCase;

class ProcessOrdersTest extends TestCase
{
    public function testCommenceEcommerceOrderProcessing()
    {
        $inputFile = "orders.jsonl";
        $saveAs = "/Users/Harish/CatchProjects/ecommerce_order_processing/public/" . $inputFile;
        $bucket = "catch-code-challenge";
        $key = "challenge-1/orders.jsonl";
        $s3Arguments = [
            'region' => $_ENV['AWS_REGION'],
            'version' => $_ENV['AWS_VERSION'],
            'credentials' => [
                'key' => $_ENV['AWS_KEY'],
                'secret' => $_ENV['AWS_SECRET'],
            ]
        ];

        $this->assertIsString($inputFile);
        $this->assertIsString($saveAs);
        $this->assertIsString($bucket);
        $this->assertIsString($key);
        $this->assertIsArray($s3Arguments);

        $AmazonS3ServiceMock = $this->getMockBuilder(AmazonS3Service::class)
            ->setMethods(['downloadFileFromAwsS3Bucket'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->assertEquals(null, $AmazonS3ServiceMock->downloadFileFromAwsS3Bucket());
    }

    public function testValidateInputFile()
    {
        $inputFile = "orders.jsonl";
        $saveAs = "/Users/Harish/CatchProjects/ecommerce_order_processing/public/" . $inputFile;
        // Assert Input file
        $this->assertFileExists($saveAs);
        $this->assertFileIsReadable($saveAs);
    }

    public function  testWriteOrders()
    {
        $outputFile = "out.csv";
        $outcsv = "/Users/Harish/CatchProjects/ecommerce_order_processing/public/" . $outputFile;
        // Assert Output file
        $this->assertFileExists($outcsv);
        $this->assertFileIsWritable($outcsv);
    }
}