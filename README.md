# Catch Coding Challenge
## Ecommerce Order Processing

This application downloads data file from AWS S3, reads the input data file, converts and processes each record, produces an out.csv data file in public directory and displays the output data on the home page.  
The commencement of the orders.jsonl file download and order processing happens when the home page is accessed. The output data will be rendered on the home page.  
This project is built with PHP programming language, Symfony framework & PHPUnit testing framework.  

## Getting Started

These instructions will get you this project up and running on your local machine for evaluation.

### Prerequisites

Before starting the installation you must have the following prerequisites.

1. **macOS Terminal/ Linux Shell Prompt:** You must have Mac Terminal access and little knowledge about working with the terminal application. Login to your Mac system and open terminal or open Linux Shell Prompt.
2. **Composer:** Composer is a tool for dependency management in PHP. It allows you to declare the libraries your project depends on and it will manage (install/update) them for you.
3. Clone GIT repository from Bitbucket in a desired local_dev_path.  
   ```
   git clone https://chanderharish1@bitbucket.org/catch-coding-challenge/ecommerce_order_processing.git
   ```  

### Installing

This section details a step by step approach that tell you how to get a development env up and running.

1. **Check Symfony Requirements:**  
      ```
      symfony check:requirements
      ```  

2. **Download & Install PHP on macOS:** [PHP - Downloads](https://www.php.net/downloads)  

    * Install PHP 7.3.x  
        ```
        curl -s http://php-osx.liip.ch/install.sh | bash -s 7.3
        ```
    * Verify PHP Installation  
        ```
        export PATH=/usr/local/php5/bin:$PATH
        ```  
        ```
        php -v
        ```  
    Once the project is up and running you can check the PHP info [here](https://localhost:8000/phpinfo.php)    

3. **Download & Install Composer:** [Composer](https://getcomposer.org/)  
    * Install Composer which is a preferred dependency management tool in Symfony . You can install packages using composer which are called **bundles** in Symfony.  
    * Please follow [Introduction - Composer](https://getcomposer.org/doc/00-intro.md) to install Composer in your local environment.  
        1. Download the installer to the current directory  
            ```
            php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
            ```
        2. Verify the installer SHA-384  
            ```
            php -r "if (hash_file('sha384', 'composer-setup.php') === 'e0012edf3e80b6978849f5eff0d4b4e4c79ff1609dd1e613307e16318854d24ae64f26d17af3ef0bf7cfb710ca74755a') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
            ```
        3. Run the installer  
            ```
            php composer-setup.php
            ```
        4. Remove the installer  
            ```
            php -r "unlink('composer-setup.php');"
            ```

        5. You can also install Composer to a specific directory by using the --install-dir option and additionally (re)name it as well using the --filename option  
            ```
            php composer-setup.php --install-dir=bin --filename=composer
            ```
        6. To install it globally run below commands.  
            ```
            mkdir -p /usr/local/bin
            ```  
            ```
            mv composer.phar /usr/local/bin/composer
            ```  
        7. Run this command if -sh: composer: command not found  
            ```
            ln -s /usr/local/bin/composer.phar /usr/local/bin/composer
            ```  
        8. Install all the bundles for the project using the below command.  
            ```
            composer install
            ```  
        9. Update necessary bundles for the project using the below command.   
            ```
            composer update
            ```

## Running Web Server for Symfony Application

1. **Start:** Symfony Local Web Server  
    ```
    symfony server:start
    ```  
    OR  
    ```
    symfony server:start -d
    ```  
2. **Stop:** Symfony Local Web Server  
    ```
    symfony server:stop
    ```  
3. **Status:** Check Symfony Local Web Server Status  
    ```
    symfony server:status
    ```

## Executing the PHPUnit Tests

1. Make sure PHPUnit bundle is included using below command  
    ```
    composer require --dev phpunit
    ```  
   OR  
   ```
    composer require --dev symfony/phpunit-bridge
   ```  
2. Run PHPUnit Tests & Assertions  
    ```
    php bin/phpunit
    ```  

## Built With

* [Symfony](https://symfony.com/) - A PHP Framework for web projects
* [Composer](https://getcomposer.org/) - A Dependency Management tool
* [PHPUnit](https://phpunit.de/) - A programmer-oriented Testing Framework

## Author

* [Harish Chander Setty Balachandran](mailto:chanderharish1@gmail.com)
