<?php

namespace App\Services;

/**
 * Class EcommerceOrder
 *
 * @package App\Services
 */
class EcommerceOrder
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $datetime;

    /**
     * @var array
     */
    private $customer;

    /**
     * @var array
     */
    private $items;

    /**
     * @var array
     */
    private $discounts;

    /**
     * @var float
     */
    private $shipping_price;

    /**
     * @param int $id
     * @param string $order_date
     * @param array $customer
     * @param array $items
     * @param array $discounts
     * @param float $shipping_price
     */
    public function __construct(int $id, string $order_date, array $customer, array $items, array $discounts, float $shipping_price)
    {
        $this->setId($id);
        $this->setDateTime($order_date);
        $this->setCustomer($customer);
        $this->setItems($items);
        $this->setDiscounts($discounts);
        $this->setShippingPrice($shipping_price);
    }

    /**
     * @return float
     */
    public function totalItemsValue()
    {
        $total_item_value = 0;
        foreach ($this->items as $item) {
            $total_item_value += $item['quantity'] * $item['unit_price'];
        }
        return $total_item_value;
    }

    /**
     * @param float $totalItemValue
     * @return float
     */
    public function applyDiscounts(float $totalItemValue)
    {
        $totalOrderValue = $totalItemValue;
        foreach ($this->discounts as $discount) {
            switch ($discount['type']) {
                case "DOLLAR":
                    if ($totalOrderValue > $discount['value'])
                        $totalOrderValue = $totalOrderValue - $discount['value'];
                    else
                        $totalOrderValue = 0;
                    break;
                case "PERCENTAGE":
                    $totalOrderValue = $totalOrderValue - ($totalOrderValue * ($discount['value'] / 100));
                    break;
            }
        }
        return round($totalOrderValue, 2);
    }

    /**
     * @return float
     */
    public function totalOrderValue()
    {
        return $this->applyDiscounts($this->totalItemsValue());
    }

    /**
     * @return int
     */
    public function numberOfUnits()
    {
        return count($this->items);
    }

    /**
     * @return float
     */
    public function averageUnitPrice()
    {
        $sum_of_unit_price = 0;
        foreach ($this->items as $item)
            $sum_of_unit_price += $item['unit_price'];
        $average_unit_price = ($sum_of_unit_price / $this->numberOfUnits());
        return round($average_unit_price, 2);
    }

    /**
     * @return int
     */
    public function distinctUnitCount()
    {
        return count(array_unique($this->items, SORT_REGULAR));
    }

    /**
     * @return int
     */
    public function totalUnitsCount()
    {
        $total_units_count = 0;
        foreach ($this->items as $item)
            $total_units_count += $item['quantity'];
        return $total_units_count;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getDateTime()
    {
        return $this->datetime;
    }

    /**
     * @param string $order_date
     */
    public function setDateTime(string $order_date)
    {
        $this->datetime = date("Y-m-d h:i:s A", strtotime($order_date));
    }

    /**
     * @return string
     */
    public function getCustomerState()
    {
        return $this->customer['shipping_address']['state'];
    }

    /**
     * @param array $customer
     */
    public function setCustomer(array $customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return array
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param array $items
     */
    public function setItems(array $items)
    {
        $this->items = $items;
    }

    /**
     * @param array $discounts
     */
    public function setDiscounts(array $discounts)
    {
        $this->discounts = $discounts;
    }

    /**
     * @return float
     */
    public function getShippingPrice()
    {
        return $this->shipping_price;
    }

    /**
     * @param float $shipping_price
     */
    public function setShippingPrice(float $shipping_price)
    {
        $this->shipping_price = $shipping_price;
    }
}