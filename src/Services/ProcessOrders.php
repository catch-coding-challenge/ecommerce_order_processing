<?php

namespace App\Services;

use Symfony\Component\Filesystem\Exception\FileNotFoundException;

class ProcessOrders
{
    /**
     * @var string
     */
    private $project_root;

    /**
     * @param string $projectRoot
     */
    public function __construct(string $projectRoot)
    {
        $this->setProjectRoot($projectRoot);
    }

    /**
     * Commence Ecommerce order processing
     *
     * @return array
     */
    public function commenceEcommerceOrderProcessing()
    {
        $fileName = "orders.jsonl";
        $saveAs = $this->getProjectRoot() . "/public/" . $fileName;
        $bucket = "catch-code-challenge";
        $key = "challenge-1/orders.jsonl";
        $s3Arguments = [
            'region' => $_ENV['AWS_REGION'],
            'version' => $_ENV['AWS_VERSION'],
            'credentials' => [
                'key' => $_ENV['AWS_KEY'],
                'secret' => $_ENV['AWS_SECRET'],
            ]
        ];

        // Download Orders JSONL file from AWS S3 Bucket to public directory
        $amazonS3Object = new AmazonS3Service($bucket, $key, $saveAs, $fileName, $s3Arguments);
        $amazonS3Object->downloadFileFromAwsS3Bucket();

        // Check input file exists in public directory and return all records
        $jsonl_records = $this->validateInputFile($saveAs);

        // Decode JSON Lines records into PHP array
        $orders = $this->decodeJsonLRecords($jsonl_records);

        // Process orders data & Create order summary list
        $summary_list = $this->processOrders($orders);

        // Write order summary 'out.csv' file
        $header = array("order_id", "order_datatime", "total_order_value", "average_unit_price",
            "distinct_unit_count", "total_units_count", "customer_state");
        $this->writeOrders($header, $summary_list);

        return $summary_list;
    }

    /**
     * Check input file exists in public directory. If exists return all records.
     *
     * @param $saveAs
     * @return array|false
     */
    public function validateInputFile($saveAs)
    {
        if (file_exists($saveAs)) {
            $jsonl_records = file($saveAs, FILE_IGNORE_NEW_LINES);
        } else {
            throw new FileNotFoundException($saveAs);
        }
        return $jsonl_records;
    }

    /**
     * Decode each JSON string into a PHP array
     *
     * @param $jsonl_records
     * @return array
     */
    public function decodeJsonLRecords($jsonl_records)
    {
        $orders = array();
        foreach ($jsonl_records as $record) {
            array_push($orders, json_decode($record, true));
        }
        return $orders;
    }

    /**
     * Process Orders
     *
     * @param $orders
     * @return array
     */
    public function processOrders($orders)
    {
        $summary_list = array();
        foreach ($orders as $order) {
            $summary_record = array();
            $ordersObject = new EcommerceOrder($order['order_id'], $order['order_date'], $order['customer'],
                $order['items'], $order['discounts'], $order['shipping_price']);
            if ($ordersObject->totalOrderValue() > 0) {
                array_push($summary_record, $ordersObject->getId());
                array_push($summary_record, $ordersObject->getDateTime());
                array_push($summary_record, $ordersObject->totalOrderValue());
                array_push($summary_record, $ordersObject->averageUnitPrice());
                array_push($summary_record, $ordersObject->distinctUnitCount());
                array_push($summary_record, $ordersObject->totalUnitsCount());
                array_push($summary_record, $ordersObject->getCustomerState());
                array_push($summary_list, $summary_record);
            }
        }
        return $summary_list;
    }

    /**
     * Write order summary into out.csv file
     *
     * @param $header
     * @param $summary_list
     */
    public function writeOrders($header, $summary_list)
    {
        $csv_file = fopen($this->getProjectRoot() . '/public/out.csv', 'w');
        fputcsv($csv_file, $header);
        foreach ($summary_list as $record) {
            fputcsv($csv_file, $record);
        }
        fclose($csv_file);
    }

    /**
     * @return string
     */
    public function getProjectRoot()
    {
        return $this->project_root;
    }

    /**
     * @param string $project_root
     */
    public function setProjectRoot(string $project_root)
    {
        $this->project_root = $project_root;
    }
}