<?php

namespace App\Services;

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

/**
 * Class AmazonS3Service
 *
 * @package App\Services
 */
class AmazonS3Service
{
    /**
     * @var S3Client
     */
    private $client;

    /**
     * @var string
     */
    private $bucket;

    /**
     * @var string
     */
    private $key;

    /**
     * @var string
     */
    private $save_as;

    /**
     * @var string
     */
    private $file_name;

    /**
     * @param string $bucket
     * @param string $key
     * @param string $saveAs
     * @param string $fileName
     * @param array $s3Arguments
     */
    public function __construct($bucket, $key, $saveAs, $fileName, $s3Arguments)
    {
        $this->setBucket($bucket);
        $this->setKey($key);
        $this->setSaveAs($saveAs);
        $this->setFileName($fileName);
        $this->setClient(new S3Client($s3Arguments));
    }

    /**
     * Download file from AWS S3 bucket
     */
    public function downloadFileFromAwsS3Bucket()
    {
        try {
            $this->getClient()->getObject([
                'Bucket' => $this->getBucket(),
                'Key' => $this->getKey(),
                'SaveAs' => $this->getSaveAs(),
                'ACL' => 'public-read',
                'ResponseContentType' => 'application/octet-stream',
                'ResponseContentDisposition' => 'attachment; filename="' . $this->getFileName() . '"',
                'ResponseCacheControl' => 'No-cache'
            ]);
        } catch (S3Exception $e) {
            echo $e->getMessage() . PHP_EOL;
        }
    }

    /**
     * @return S3Client
     */
    protected function getClient()
    {
        return $this->client;
    }

    /**
     * @param S3Client $client
     */
    private function setClient(S3Client $client)
    {
        $this->client = $client;
    }

    /**
     * @return string
     */
    protected function getBucket()
    {
        return $this->bucket;
    }

    /**
     * @param string $bucket
     */
    private function setBucket($bucket)
    {
        $this->bucket = $bucket;
    }

    /**
     * @return string
     */
    protected function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $key
     */
    private function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @return string
     */
    protected function getSaveAs()
    {
        return $this->save_as;
    }

    /**
     * @param string $save_as
     */
    private function setSaveAs(string $save_as)
    {
        $this->save_as = $save_as;
    }

    /**
     * @return string
     */
    protected function getFileName()
    {
        return $this->file_name;
    }

    /**
     * @param string $file_name
     */
    private function setFileName(string $file_name)
    {
        $this->file_name = $file_name;
    }
}