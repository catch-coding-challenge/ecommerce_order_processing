<?php

namespace App\Controller;

use App\Services\ProcessOrders;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * Render order summary in Home Page
     *
     * @Route("/", name="render_home_page")
     */
    public function renderHomePageAction()
    {
        $projectRootDir = $this->getParameter('kernel.project_dir');
        $orderProcessing = new ProcessOrders($projectRootDir);
        $summary_list = $orderProcessing->commenceEcommerceOrderProcessing();
        return $this->render('resources/views/index.html.twig', array('summaryList' => $summary_list));
    }
}